namespace EVEMon.Common.CloudStorageServices
{
    public sealed class CloudStorageServiceAPICredentials
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>
        /// The user identifier.
        /// </value>
        public uint UserID { get; set; }
    }
}