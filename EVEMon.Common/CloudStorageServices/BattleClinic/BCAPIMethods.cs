﻿namespace EVEMon.Common.CloudStorageServices.BattleClinic
{
    public enum BCAPIMethods
    {
        None,
        CheckCredentials,
        FileGet,
        FileGetByName,
        FileSave,
    }
}