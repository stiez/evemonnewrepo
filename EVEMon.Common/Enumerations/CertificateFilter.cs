﻿namespace EVEMon.Common.Enumerations
{
    public enum CertificateFilter
    {
        All = 0,
        Completed = 1,
        HideMaxLevel = 2,
        NextLevelTrainable = 3,
        NextLevelUntrainable = 4
    }
}
