namespace EVEMon.Common.Constants
{
    public static class EVEMonConstants
    {
        public const int ImplantSetNameMaxLength = 128;
        public const string UnknownText = "Unknown";
        public const string DowntimeText = "DOWNTIME";
        public const string CurrentSkillQueueText = "Current Skill Queue";
    }
}